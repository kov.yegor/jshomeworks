var array = ['Element1', 'Element2',null, 1,2,3,'Element3', 'Element4',null];
var array2 = array.slice();
var array3 = array.slice();

function filterBy(array,dataType) {
    var resultArray = array.slice();
    for (var i = 0; i < resultArray.length; i++) {
        if(typeof resultArray[i] == dataType){
            resultArray.splice(i,1);
            i--;
        }
    }
    return resultArray;
}

var changedArray = filterBy(array,'string');
var changedArray2 = filterBy(array2,'number');
var changedArray3 = filterBy(array3,'object');
console.log(changedArray);
console.log(changedArray2);
console.log(changedArray3);