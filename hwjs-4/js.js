/*Метод обьекта это варианты действий с обьектами*/ 
class User{
    constructor(firstName,lastName){
        this.lastName = lastName;
        this.firstName = firstName;
    }
    getLogin() {
        return this.firstName.toLowerCase() + this.lastName.toLowerCase();
    }
}

function createNewUser() {
    return new User(prompt("First name?"), prompt("Last name?"));
}

var newUser = createNewUser();

console.log(newUser.getLogin());