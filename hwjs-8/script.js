let priceInput = document.getElementById("priceInput");
let popSpans = document.getElementById("popSpans");

priceInput.addEventListener('focusin', (event) => {
    event.target.style.border = '2px solid rgb(0, 200, 0)';
    event.target.style.backgroundColor='rgb(240, 255, 240)';
});

priceInput.addEventListener('focusout', (event) => {
    event.target.style.border = '2px solid rgb(0, 0, 0)';
    event.target.style.backgroundColor='#fff';
    priceInput.placeholder='Enter your price';
    createPopSpan(event.target.value);
    event.target.value = '';
});

function createPopSpan(content) {
    if(content!=''){
            if(!isNaN(content)){
            if(content>0){
                priceInput.style.backgroundColor='#fff';
                let popSpan = document.createElement("span");
                let spanText = document.createElement("p");
                let spanX = document.createElement("div");
                popSpans.appendChild(popSpan);
                popSpan.appendChild(spanText);
                popSpan.appendChild(spanX);
                popSpan.className="popSpan";
                spanText.className="spanText";
                spanX.className="spanX";
                spanText.textContent='$' + content;
                spanX.onclick = function(){
                    this.parentElement.remove();
                }
                spanX.onmouseover = function() {
                    this.parentElement.style.backgroundColor="#ffeeee";
                    this.parentElement.style.border="1.5px solid #ffaaaa";
                };
                spanX.onmouseout = function() {
                    this.parentElement.style.backgroundColor="#fff";
                    this.parentElement.style.border="1.5px solid #888";
                };

            } else{
                priceInput.focus();
                priceInput.placeholder='Please enter correct price';
                priceInput.style.backgroundColor='#ffdddd';
                event.target.style.border = '2px solid rgb(244, 0, 0)';
            }
        } else{
            priceInput.focus();
            priceInput.placeholder='Please enter correct price';
            priceInput.style.backgroundColor='#ffdddd';
            event.target.style.border = '2px solid rgb(244, 0, 0)';
        }
    }
}